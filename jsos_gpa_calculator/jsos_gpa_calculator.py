import argparse
import getpass
from dataclasses import dataclass

from browser.jsos_browser import JSOSBrowser
from analyze.gpa import GPA
from display.printer import print_results
from display.graph import draw_graph


@dataclass
class Credentials:
    username: str
    password: str

    def __iter__(self):
        return iter((self.username, self.password))


def login_menu():
    username = input('\nUsername: ')
    password = getpass.getpass()
    return Credentials(username, password)


def jsos_gpa_calculator():
    args = parse_args()
    if args.username is None or args.password is None:
        credentials = login_menu()
    else:
        credentials = Credentials(args.username, args.password)
    browser = JSOSBrowser(*credentials, not args.noheadless)
    gpa = GPA()
    marks_source = browser.get_marks_source()
    gpa.load(marks_source)
    marks = gpa.calculate_all()
    print_results(marks)
    if args.graph:
        draw_graph(marks)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('username', metavar='username', type=str, help='JSOS username', nargs='?')
    parser.add_argument('--noheadless', action='store_true', default=False,
                        help='run in non-headless mode (launch in a browser)')
    parser.add_argument('--graph', '-g', action='store_true', default=False, help='draw a GPA over semesters graph')
    parser.add_argument('password', metavar='password', type=str, help='JSOS password', nargs='?')
    args, unknown = parser.parse_known_args()
    return args


if __name__ == '__main__':
    jsos_gpa_calculator()
