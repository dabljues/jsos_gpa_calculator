from bs4 import BeautifulSoup
from dataclasses import dataclass
from os.path import abspath
from humanfriendly.tables import format_pretty_table

CODE_INDEX = 1
NAME_INDEX = 2
MARK_INDEX = -3
ECTS_INDEX = -1


@dataclass
class Mark:
    name: str
    code: str
    mark: float
    ects: int


@dataclass
class Result:
    gpa: float
    complete: bool


@dataclass
class SemesterResult:
    number: int
    result: Result

    def __iter__(self):
        return iter((self.number, self.result.gpa, self.result.complete))


class GPA:
    def __init__(self):
        self.filename = ''
        self.soup = None

    def load_from_file(self, filename):
        self.filename = abspath(filename)
        self.soup = BeautifulSoup(open(self.filename, encoding='utf-8'), 'html.parser')

    def load(self, content):
        self.soup = BeautifulSoup(content, 'html.parser')

    def _get_rows(self, semester):
        rows = semester.find_all('tr')
        return rows

    def _get_semesters(self):
        tables = self.soup.find_all('tbody', attrs={"class": "semester"})
        return tables

    def calculate_all(self):
        results = [SemesterResult(i + 1, self.calculate(semester))
                   for i, semester
                   in enumerate(reversed(self._get_semesters()))]
        return results

    def calculate(self, semester):
        rows = self._get_rows(semester)
        marks = []
        all_marks = True

        for row in rows:
            cols = row.find_all("td")
            code = cols[CODE_INDEX].string
            name = cols[NAME_INDEX].string
            mark = cols[MARK_INDEX].string
            mark = mark.replace('(E)', '')
            try:
                mark = float(mark)
            except ValueError:
                all_marks = False
                continue
            ects = cols[ECTS_INDEX].string
            try:
                ects = int(ects)
            except (ValueError, TypeError):
                continue
            m = Mark(name, code, mark, ects)
            marks.append(m)

        s = sum((m.ects * m.mark for m in marks))
        ects_count = sum((m.ects for m in marks))
        if ects_count == 0:
            average = 0.00
        else:
            average = round(s / ects_count, 2)
        return Result(average, 'Yes') if all_marks else Result(average, 'No')
