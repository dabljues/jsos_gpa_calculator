from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from time import sleep

LOGIN_URL = 'https://jsos.pwr.edu.pl/index.php/site/loginAsStudent'
MARKS_URL = 'https://jsos.pwr.edu.pl/index.php/student/indeksOceny/oceny/1'


class JSOSBrowser:
    def __init__(self, username, password, headless=True):
        self.driver = webdriver.Chrome(options=self.set_options(headless))
        self.username = username
        self.password = password

    def set_options(self, headless):
        chrome_options = Options()
        if headless:
            chrome_options.add_argument("--headless")
        chrome_options.add_argument('log-level=3')
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument('test-type')
        return chrome_options

    def _login(self):
        self.driver.get(LOGIN_URL)
        username_textbox = self.driver.find_element_by_id('username')
        username_textbox.clear()
        username_textbox.send_keys(self.username)
        password_textbox = self.driver.find_element_by_id('password')
        password_textbox.clear()
        password_textbox.send_keys(self.password)
        login_button = self.driver.find_element_by_id('authenticateButton2')
        login_button.click()

    def _display_all_marks(self):
        self.driver.get(MARKS_URL)
        expand_button = self.driver.find_element_by_id('expand')
        sleep(1)
        webdriver.ActionChains(self.driver).move_to_element(expand_button).click(expand_button).perform()
        sleep(2)

    def get_marks_source(self):
        self.driver.maximize_window()
        self._login()
        self._display_all_marks()
        return self.driver.page_source
