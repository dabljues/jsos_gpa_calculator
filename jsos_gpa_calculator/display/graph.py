import matplotlib.pyplot as plt


def draw_graph(results):
    x = [result.number for result in results]
    y = [result.result.gpa for result in results]
    plt.plot(x, y, '-o')
    plt.grid(linestyle='--')
    plt.xlabel('Semester')
    plt.xlim(1, len(results))
    plt.ylabel('GPA')
    plt.title(f'GPA over past/current semesters', loc='center')
    plt.show()
