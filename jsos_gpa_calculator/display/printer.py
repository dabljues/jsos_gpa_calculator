from humanfriendly.tables import format_pretty_table

COLUMN_NAMES = ['Semester no.', 'GPA', 'Complete']


def print_results(results):
    print(format_pretty_table(results, COLUMN_NAMES))
